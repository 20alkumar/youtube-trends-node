import Axios from 'axios';
import * as config from '../config.json';
import moment from "moment";
import async from 'async';

const axios = Axios.create({
  baseURL: config.youtubeApi.endpoint
});

export class YoutubeService {
  getTrendingVideos(callback) {
    var params = {
      part: 'snippet',
      chart: 'mostPopular',
      regionCode: 'IN', // should be replaced with country code from countryList
      maxResults: '24',
      key: config.youtubeApi.key
    };
    var result = [];
    axios.get('/', {params}).then(function(res) {
      console.log('res.data.items[0]===>', JSON.stringify(res.data.items[0]));
      
      async.forEach(res.data.items, (data, dataCallback) => {
        let video = {
          id: data.id,
          title: data.snippet.title,
          thumbnail: data.snippet.thumbnails.standard.url,
          publishedAt: moment(data.snippet.publishedAt).fromNow()
        };
        YoutubeService.getVideoDetails(video, (response) => {
          result.push(response);
          dataCallback();  
        });
      }, (err, done) => {
        console.log('result===>', result);
        return callback(result);
      });
    });
  }

  static getVideoDetails(video, cb) {
    var params = {
      part: 'statistics',
      id: video.id,
      key: config.youtubeApi.key
    };

    return axios.get('/', {params}).then(function(res) {
      var result = res.data;
      video.viewCount = result['items'][0].statistics.viewCount;
      video.likeCount = result['items'][0].statistics.likeCount;
      return cb(video);
    });
  }
}
